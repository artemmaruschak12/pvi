const socket = io("http://localhost:4000", { autoConnect: false });
const messages = {};

function initSocket(userName)
{
    socket.on("users", onUsers);
    socket.on("user connected", onUserConnected);
    socket.on("join request", onJoinRequest);
    socket.on("incoming message", onIncomingMessage);

    socket.onAny((event, ...args) => {
        console.log(event, args);
    });

    document.querySelector("#username-display").text = userName;
    socket.auth = {username: userName};
    socket.connect();
}

function onIncomingMessage({from, to, content})
{
    if (!messages[to])
        messages[to] = [];
    messages[to].push({from, content});
    console.log(messages);

    const selectedChat = document.querySelector(".selected-box");
    if(!selectedChat)
        return;
    
    if(selectedChat.getAttribute("data-id") == to)
    {
        document.querySelector("#messages-container").innerHTML += buildMessage({from, content});
    }
}

function onJoinRequest(joinRequest)
{
    console.log(createChatBlock(joinRequest));
    document.querySelector("#chats").innerHTML += createChatBlock(joinRequest);
    socket.emit("joined chat", {chatUUID: joinRequest.chatUUID});
}


function saveCurrentUser(event)
{
    event.preventDefault();
    var userName = document.querySelector("#username-input").value;
    initSocket(userName);
    document.querySelector("#username-prompt").classList.add("fall-down");
}

function initiateCreatingChat()
{
    const frame = document.querySelector("#creating-chat-frame");
    if(frame.classList.contains("hidden"))
        frame.classList.remove("hidden");
    frame.classList.remove("fall-down");
    frame.classList.add("rise-up");
}

function closeChatCreatingModal()
{
    const frame = document.querySelector("#creating-chat-frame");
    frame.classList.add("fall-down");
    frame.classList.remove("rise-up");
}

function sendChatCreatingRequest()
{
    const input = document.querySelector("#chatname-input");
    const userList = document.querySelectorAll("#students-list > .student-item");

    const chatname = input.value;
    if(!chatname)
    {
        closeChatCreatingModal()
        return;
    }
        

    const participants = [];
    userList.forEach(userItem => {
        const checkbox = userItem.querySelector("input");
        if(!checkbox.checked)
            return;
    
        const id = userItem.getAttribute("data-id");
        const username = userItem.getAttribute("data-username");
        
        participants.push({id, username});
    })

    if(participants.length === 0)
    {
        closeChatCreatingModal()
        return;
    }

    participants.push({id: socket.id, username: socket.auth.username});
    socket.emit("create chat", {chatname, participants});
    console.log(participants);
    closeChatCreatingModal();
}

function generateUserItem(user)
{
    return `
    <div class="student-item" data-id=${user.id} data-username=${user.username}>
        <input type="checkbox"/>
        <span>${user.username}</span>
    </div>`;
}

function createChatBlock(chat)
{
    return `
        <div class="chat-box" data-id="${chat.chatUUID}" onclick="setChatBoxAsSelected(this)">
            <img src="static/images/user.jpg"/>
            <span>${chat.chatname}</span>
        </div>`;
}

function onUsers(users)
{
    const myUserName = socket.auth.username;
    const html = users.filter(user => user.username !== myUserName).map(generateUserItem).join();
    document.querySelector("#students-list").innerHTML = html;
}

function onUserConnected(user)
{
    document.querySelector("#students-list").innerHTML += generateUserItem(user);
}

function setChatBoxAsSelected(box)
{
    document.querySelectorAll(".chat-box").forEach(box => {
        box.classList.remove("selected-box");
    });

    box.classList.add("selected-box");
    const id = box.getAttribute("data-id");

    document.querySelector("#messages-container").innerHTML = buildMessagesHistoryFor(id);
}

function buildMessagesHistoryFor(chatId)
{
    if(!messages[chatId])
        return "";
    return messages[chatId].map(buildMessage).join("");
}

function buildMessage(message)
{
    const className = message.from === socket.auth.username ? "own-message" : "message";

    return `
        <div class="${className}">
            <div class="message-header">
                ${message.from}
            </div>
            <div class="message-body">
                ${message.content}
            </div>
        </div>`;
    
}

function processMessageForm(event)
{
    event.preventDefault();

    const input = document.querySelector("#message-input");
    const content = input.value;

    if(!content.trim())
        return;

    const selectedChat = document.querySelector(".selected-box");
    if(!selectedChat)
        return;
    
    const to = selectedChat.getAttribute("data-id");
    const from = socket.auth.username;

    socket.emit("send message", {from, to, content});
    input.value = "";

    if(!messages[to])
        messages[to] = [];
    messages[to].push({from, content});
    console.log(messages);
    document.querySelector("#messages-container").innerHTML += buildMessage({from, content});
}