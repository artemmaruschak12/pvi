const route = (event) => {
    event = event || window.event;
    event.preventDefault();
    window.history.pushState({}, "", event.target.href);

    handleLocation();
};

const ids = {
    "/": "mainPage",
    "/chat": "chatPage",
};

const handleLocation = async () => {
    const path = window.location.pathname;
    const _id = ids[path] || ids["/"];
    
    document.getElementById(_id).classList.remove("hidden");
    Object.values(ids).forEach(i => {
        if(i !== _id) document.getElementById(i).classList.add("hidden");
    });

    document.querySelectorAll("#dropdown > a").forEach(link => link.classList.remove("active-page"));
    const elem = document.querySelector(`a[href="${path}"]`);
    elem.classList.add("active-page");
};

window.onpopstate = handleLocation;
window.route = route;

handleLocation();
