let studentsList = [];
let student_id = 0;
let page = 1;
const PAGE_SIZE = 4;
let _isEditMode = false;
let genders = null;
let groups = null;

function closeConfirmationWindow()
{
    document.querySelector('.confirmation-frame').classList.add('hidden');
}

function openConfirmationWindow(id)
{
    document.querySelector('.confirmation-frame').classList.remove('hidden');
    const student = studentsList.find(s => s.student_id == id);
    document.querySelector('.confirmation-body > span').innerHTML = `Are you sure that you want to delete student of group ${groups[student.group]} ${student.first_name} ${student.last_name}?`;
    document.querySelector('#yes-btn').onclick = () => deleteStudent(id);
}

async function deleteStudent(id)
{
    const response = await processDeleteCall(id);
    if(response.status)
    {
        studentsList = studentsList.filter(student => student.student_id != id);
        renderMain();
    }
    closeConfirmationWindow();
}

function addStudent(student)
{
    studentsList.push(student);
    renderMain();
}

function editStudent(student)
{
    const index = studentsList.findIndex(s => s.student_id == student.student_id);
    studentsList[index] = student;
    renderMain();
}

function createStudent(student_id, name, group, gender, birthdate, status)
{
    return {
        student_id,
        name,
        group, 
        gender, 
        birthdate, 
        status
    };
}

function getStudentsPage(page_number)
{
    return studentsList.slice((page_number-1) * PAGE_SIZE, page_number * PAGE_SIZE);
}

function setPage(page_num)
{
    page = page_num;
    renderMain();
}

function getPagesCount()
{
    return Math.ceil(studentsList.length / PAGE_SIZE);
}

function getStudentsTableHtml(studentsList)
{
    let tableHtml = `<table id="students-table">
    <thead> 
        <tr>
            <th>
                <input type="checkbox"/>
            </th>
            <th>Group</th>
            <th>Name</th>
            <th>Gender</th>
            <th>Birthday</th>
            <th>Status</th>
            <th>Options</th>
        </tr>
    </thead>
    <tbody>`;



    getStudentsPage(page).forEach(student => 
        {
            tableHtml += `<tr>
            <td>
                <input type="checkbox"/>
            </td>
            <td>${groups[student.group]}</td>
            <td>${student.first_name} ${student.last_name}</td>
            <td>${genders[student.gender]}</td>
            <td>${student.birthdate}</td>
            <td>
                <div class="${student.status === 1 ? "status-online" : "status-offline"}"></div>
            </td>
            <td>
                <div class="options-container">
                    <button onclick="showModal(true, ${student.student_id});">
                        <i class="fa-regular fa-pen-to-square"></i>
                    </button>
                    <button onclick="openConfirmationWindow(${student.student_id});">
                        <i class="fa-regular fa-circle-xmark"></i>
                    </button>
                </div>
            </td>
            </tr>`;
        });

    tableHtml += `</tbody>
    </table>`;

    return tableHtml;
}

function addNotificationListener()
{
    document.querySelector(".header-notification-container").onclick = () =>
        document.querySelector("#header-notification-indicatior").classList.toggle("active-notification");
}

function addClickEventListenerToDropdown()
{
    document.querySelector("#dropdown-btn").onclick = () =>
    {
        document.querySelector("#dropdown").classList.toggle("active-dropdown");
    };
}

function addOpacityTransitionOnAllPopups()
{
    document.querySelectorAll(".popup").forEach(elem =>
        {
            elem.classList.toggle("transition-opacity");
        });
}

function renderTable()
{
    document.querySelector(".table-scroller").innerHTML = getStudentsTableHtml(studentsList);
}

function renderPagination()
{
    const container = document.querySelector(".pagination-container");
    container.innerHTML = '<button onclick="setPage(1);"><<</button>';
    container.innerHTML += '<button onclick="if(page > 1) setPage(page - 1);"><</button>';
    for(i = 1; i <= getPagesCount(); i++)
    {
        container.innerHTML += `<button onclick="setPage(${i})">${i}</button>`;
    }
    container.innerHTML += '<button onclick="if(page < getPagesCount()) setPage(page + 1)">></button>';
    container.innerHTML += '<button onclick="setPage(getPagesCount())">>></button>';
}

function renderMain()
{
    if(page < 1) page = 1;
    if(page > getPagesCount()) page = getPagesCount();
    renderTable();
    renderPagination();
}

async function getStudentsFromServer()
{
    var res = await fetch("/api/v1/student");
    var json = await res.json();
    console.log(json.user);
    studentsList.push(...json.user);
}

async function getGendersFromServer()
{
    var res = await fetch("/api/v1/gender");
    var json = await res.json();
    console.log(json.genders);
    genders = json.genders;
}

async function getGroupsFromServer()
{
    var res = await fetch("/api/v1/group");
    var json = await res.json();
    console.log(json.groups);
    groups = json.groups;
}

function removeLoader()
{
    
    document.querySelector(".loader-frame").classList.remove("rise-up");
    document.querySelector(".loader-frame").classList.add("fall-down");
}

function showLoader()
{
    document.querySelector(".loader-frame").classList.remove("fall-down");
    document.querySelector(".loader-frame").classList.add("rise-up");
}

function renderSelectsOnForm()
{
    let select = document.querySelector("select[name='group']");

    for(const [group_num, group_name] of Object.entries(groups))
    {
        select.innerHTML += `<option value="${group_num}">${group_name}</option>`;
    }

    select = document.querySelector("select[name='gender']");

    for(const [gender_num, gender_name] of Object.entries(genders))
    {
        select.innerHTML += `<option value="${gender_num}">${gender_name}</option>`;
    }
}

async function performActionsOnWindowLoad()
{
    await Promise.all(
        [getStudentsFromServer(),
         getGendersFromServer(),
         getGroupsFromServer()]);

    addClickEventListenerToDropdown();
    addNotificationListener();
    renderMain();
    renderSelectsOnForm();
    removeLoader();
}

function showErrorInModal(message)
{
    const errorLabel = document.querySelector(".error-message");
    errorLabel.classList.remove("hidden");
    errorLabel.innerHTML = message;
}

function hideErrorInModal()
{
    const errorLabel = document.querySelector(".error-message");
    errorLabel.classList.add("hidden");
}

async function processDeleteCall(id)
{
    const request = await fetch(
        `/api/v1/student/${id}`,
        { 
            "method":"DELETE"
        });
    
    return await request.json();  
}

async function processAddCall(student)
{ 
    const request = await fetch(
        "/api/v1/student",
        { 
            'method' : "POST",
            "headers" : 
            {
                "Content-Type":"application-json"
            },
            "body":JSON.stringify(student)
        });
    
    return await request.json();   
}

async function processEditCall(data)
{ 
    const request = await fetch(
        "/api/v1/student",
        { 
            "method":"PUT",
            "headers" : 
            {
                "Content-Type":"application-json"
            },
            "body":JSON.stringify(data)
        });
    
    return await request.json();   
}

async function trySubmitForm(e)
{
    e.preventDefault();

    showLoader();
    const data = new FormData(e.target);
    const student = Object.fromEntries(data.entries());
    
    student.student_id = +student.student_id;
    student.group = +student.group;
    student.gender = +student.gender;
    student.status = +student.status;

    let json = null;

    if(_isEditMode)
    {
        const old_student = studentsList.find(s => s.student_id == student.student_id);

        let is_updated = false;
        let updated_data = {student_id: student.student_id};

        for(const property in student)
        {
            if(old_student[property] !== student[property])
            {
                is_updated = true;
                updated_data[property] = student[property];
            }
                
        }
        if(is_updated)
            json = await processEditCall(updated_data);
    }
    else
    {
        json = await processAddCall(student);
    }
        

    removeLoader();

    if(!json.status)
    {
        showErrorInModal(json.error.message);
        return;
    }

    console.log(json.user);

    if(_isEditMode) editStudent(json.user);
    else addStudent(json.user);
    
    
    hideModal();
}

function showModal(isEditMode, id = -1)
{
    _isEditMode = isEditMode;

    document.querySelector("#modal-header-text").innerHTML = isEditMode ? "Edit student" : "Add student";
    document.querySelector('input[name="student_id"]').value = id; 
    document.querySelector(".modal-container").classList.remove("hidden");

    if(isEditMode)
    {
        const student = studentsList.find(s => s.student_id == id);
        const { elements } = document.querySelector('#modal-form');

        for (const [ key, value ] of Object.entries(student) ) {
            const field = elements.namedItem(key);
            field && (field.value = value);
        }
    }
}

function hideModal()
{
    //document.querySelectorAll("#modal-form > input").forEach(elem => elem.value = '');
    document.querySelector(".modal-container").classList.add("hidden");
    hideErrorInModal();
}

window.onload = performActionsOnWindowLoad;