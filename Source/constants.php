<?php

    $groups = [
        1 => "PZ-21",
        2 => "PZ-22",
        3 => "PZ-23",
        4 => "PZ-24",
        5 => "PZ-25",
        6 => "PZ-26"
    ];

    $statuses = [
        1 => "OK",
        2 => "Blocked"
    ];

    $genders = [
        1 => "Male",
        2 => "Female"
    ];

    define('S_GROUPS', $groups);
    define('S_STATUSES', $statuses);
    define("S_GENDERS", $genders);

?>