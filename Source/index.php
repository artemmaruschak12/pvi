<?php
    
    require_once ("./application.php");
    require_once ("./response.php");
    require_once ("./database_functions.php");
    require_once ("./student.php");
    require_once ("./constants.php");

    $a = new Application();

    $a->addMiddleware(function() {
        header("Content-Type: application/json");
    });

    $a->addEndpointHandler("/api/v1/gender", "GET", function($params)
    {
        echo ok_genders(S_GENDERS);
    });

    $a->addEndpointHandler("/api/v1/group", "GET", function($params)
    {
        echo ok_groups(S_GROUPS);
    });
    
    $a->addEndpointHandler("/api/v1/student", "GET", function($params)
    {
        $students = getAllStundents();

        if(!$students)
        {
            echo error(8, "Connection error. Try later");
        }
        
        $students = array_map(function($s){return $s->as_array();}, $students);

        echo ok_user($students);
    });

    $a->addEndpointHandler("/api/v1/student", "POST", function($params, $data)
    {
        try {
            $student = Student::from_json($data);
        } catch (Exception $th) {
            echo error($th->getMessage(), getCode());
        }
        
        $currentDate = date("Y-m-d");

        if (!array_key_exists($student->get_group(), S_GROUPS))
        {
            echo error(2, "Student group is necessary!");
            return;
        }
        if(!array_key_exists($student->get_gender(), S_GENDERS))
        {
            echo error(3, "Student gender is necessary!");
            return;
        }

        if(!preg_match("#^[a-zA-Z]{2,20}$#", $student->get_first_name()))
        {
            echo error(4, "Student name is invalid!<br/>First name should be 2-20 characters long!");
            return;
        }
        if(!preg_match("#^[a-zA-Z]{3,30}$#", $student->get_last_name()))
        {
            echo error(5, "Student name is invalid!<br/>Last name should be 3-30 characters long!");
            return;
        }
        if(!$student->get_status() || $student->get_status() === 0)
        {
            echo error(6, "Student status is necessary!");
            return;
        }
        if(!strlen($student->get_birthdate()) || $currentDate <= $student->get_birthdate())
        {
            echo error(7, "Invalid date of birth!");
            return;
        }

        $id = addStudent($student);
        if($id != -1)
        {
            echo ok_user($student->as_array());
        }
        else
            echo error(8, "Connection error. Try later");
            
    });

    $a->addEndpointHandler("/api/v1/student", "PUT", function($params, $data)
    {
        $data = json_decode($data);

        $currentDate = date("Y-m-d");

        if (property_exists($data, 'group') && !array_key_exists($data->group, S_GROUPS))
        {
            echo error(2, "Student group is invalid!");
            return;
        }
        if(property_exists($data, 'gender') && !array_key_exists($data->gender, S_GENDERS))
        {
            echo error(3, "Student gender is invalid!");
            return;
        }

        if(property_exists($data, 'first_name') && !preg_match("#^[a-zA-Z]{2,20}$#", $data->first_name))
        {
            echo error(4, "Student name is invalid!<br/>First name should be 2-20 characters long!");
            return;
        }
        if(property_exists($data, 'last_name') && !preg_match("#^[a-zA-Z]{3,30}$#", $data->last_name))
        {
            echo error(5, "Student name is invalid!<br/>Last name should be 3-30 characters long!");
            return;
        }
        if(property_exists($data, 'status') && !array_key_exists($data->status, S_STATUSES))
        {
            echo error(6, "Student status is invalid!");
            return;
        }
        if(property_exists($data, 'birthdate') && (!strlen($data->birthdate) || $currentDate <= $data->birthdate))
        {
            echo error(7, "Invalid date of birth!");
            return;
        }

        $student = editStudent((array)$data);
        if($student)
            echo ok_user($student->as_array());
        else
            echo error(8, "Connection error. Try later");  
    });

    $a->addEndpointHandler("/api/v1/student/{id}", "DELETE", function($params, $data)
    {
        if(deleteStudent($params['id']))
            echo ok_user(null);
        else echo error(1, "Invalid student's id!");
    });

    $a->setBaseHandler(function($endpoint)
    {
        header("Content-Type: text/html");
        readfile("index.html");
    });

    $a->handleRequest();
?>