<?php

    function error($code, $message)
    {
        return json_encode([
            "status" => false,
            "error" => [
                "message" => $message,
                "code" => $code
            ],
            "user" => null
        ]);
    }

    function ok_user($object)
    {
        return json_encode([
            "status" => true,
            "user" => $object,
            "error" => null
        ]);
    }

    function ok_groups($object)
    {
        return json_encode([
            "status" => true,
            "groups" => $object,
            "error" => null
        ]);
    }

    function ok_genders($object)
    {
        return json_encode([
            "status" => true,
            "genders" => $object,
            "error" => null
        ]);
    }


    function redirectTo($webPage)
    {
        header("Location: ".$webPage);
    }
?>