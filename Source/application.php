<?php

class Application
{
    private $params;
    private $path;
    private $endpointHandlers = array();
    private $middlewares = array();

    private $baseHandler;

    public function __construct()
    {
        $this->setBaseHandler(function() {});
    }

    public function handleRequest()
    {
        foreach ($this->middlewares as $middleware) {
            call_user_func($middleware);
        }

        $callRoute = $_GET["apiCall"];
        $method = $_SERVER["REQUEST_METHOD"];

        foreach($this->endpointHandlers as $route => $methodAndHandler)
        {
            if(preg_match_all($route, $callRoute, $params))
            {
                if(array_key_exists($method, $methodAndHandler))
                {
                    $params = array_filter( $params, function($n) {return !is_numeric($n);}, ARRAY_FILTER_USE_KEY);
                    $params = array_map(function($n) {return $n[0];}, $params); 
                    
                    if($method === 'GET')
                        $methodAndHandler[$method]($params);
                    else
                    {
                        $json = file_get_contents('php://input');
                        $methodAndHandler[$method]($params, $json);
                    }

                    return;
                }

                else
                {
                    http_response_code(405);
                    echo error(9, "Method is not allowed");
                    return;
                }  
            }
        }

        call_user_func_array($this->baseHandler, [$callRoute]);
    }

    public function addEndpointHandler($endpoint, $method, $handler)
    {
        $route_pattern = preg_replace("#{(\w+)}#", "(?P<$1>\w+)", $endpoint); // /api/v1/students/{id}
        $route_pattern = "#^".$route_pattern."$#";
        $this->endpointHandlers[$route_pattern][$method] = $handler;  
    }

    public function addMiddleware($middleware)
    {
        $this->middlewares[] = $middleware;
    }

    public function setBaseHandler($baseHandler)
    {
        $this->baseHandler = $baseHandler;
    }
}
?>