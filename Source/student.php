<?php
    require_once('./database_functions.php');
    require_once('./errors.php');
    class Student
    {
        private $student_id;
        private $first_name;
        private $last_name;
        private $group;
        private $gender;
        private $status;
        private $birthdate;

        public function __construct($id, $first_name, $last_name, $group, $gender, $status, $birthdate)
        {
            $this->set_student_id($id);
            $this->set_first_name(escapedString($first_name));
            $this->set_last_name(escapedString($last_name));
            $this->set_group($group);
            $this->set_gender($gender);
            $this->set_status($status);
            $this->set_birthdate(escapedString($birthdate));
        }

        #region Accessors

        public function get_student_id()
        {
            return $this->student_id;
        }

        public function get_first_name()
        {
            return $this->first_name;
        }

        public function get_last_name()
        {
            return $this->last_name;
        }

        public function get_group()
        {
            return $this->group;
        }

        public function get_gender()
        {
            return $this->gender;
        }

        public function get_status()
        {
            return $this->status;
        }

        public function get_birthdate()
        {
            return $this->birthdate;
        }

        public function set_student_id($value)
        {
            if(!isset($value) || !is_numeric($value))
                throw new Exception(get_error_by_code(1), 1);
                
            $this->student_id = $value;
        }

        public function set_first_name($value)
        {
            if(!isset($value))
            throw new Exception(get_error_by_code(4), 4);
            $this->first_name = $value;
        }

        public function set_last_name($value)
        {
            if(!isset($value))
                throw new Exception(get_error_by_code(5), 5);
            $this->last_name = $value;
        }

        public function set_group($value)
        {
            if(!isset($value) || !is_numeric($value))
                throw new Exception(get_error_by_code(2), 2);
            $this->group = $value;
        }

        public function set_gender($value)
        {
            if(!isset($value) || !is_numeric($value))
                throw new Exception(get_error_by_code(3), 3);
            $this->gender = $value;
        }

        public function set_status($value)
        {
            if(!isset($value) || !is_numeric($value))
                throw new Exception(get_error_by_code(6), 6);
            $this->status = $value;
        }

        public function set_birthdate($value)
        {
            if(!isset($value))
                throw new Exception(get_error_by_code(7), 7);
            $this->birthdate = $value;
        }

        #endregion
    
        public static function from_json($json)
        {
            $data = json_decode($json);
            if(!property_exists($data, 'student_id')) $data->student_id = 0;

            return new Student(
                $data->student_id,
                $data->first_name,
                $data->last_name,
                $data->group,
                $data->gender,
                $data->status,
                $data->birthdate
            );
        }

        public function as_array()
        {
            return [
                'student_id' => $this->student_id,
                'first_name' => $this->first_name,
                'last_name' => $this->last_name,
                'group' => $this->group,
                'gender' => $this->gender,
                'status' => $this->status,
                'birthdate' => $this->birthdate
            ];
        }
    }
?>