<?php
    require_once('./student.php');

    $conn = new mysqli("p:localhost", "root", "");
    $conn->select_db("students_db");

    function addStudent($student)
    {
        $first_name = $student->get_first_name();
        $last_name = $student->get_last_name();
        $gender = $student->get_gender();
        $group = $student->get_group();
        $status = $student->get_status();
        $birthdate = $student->get_birthdate();
        global $conn;
        
        $q = "INSERT INTO Students (first_name, last_name, gender, `group`, `status`, birthdate)
        VALUES ('$first_name','$last_name',$gender,$group,$status,'$birthdate');";
        if($conn->query($q))
        {
            $student->set_student_id($conn->insert_id);
            return $student;
        }
        else
        {
            return -1;
        }
    }

    function editStudent($updatedData)
    {
        global $conn;
        
        $set_queries = [];
        foreach ($updatedData as $key => $value) {
            if($key === 'student_id')
                continue;
            else if($key === 'group' || $key === 'status')
                $set_queries[] = "`$key`=$value";
            else if(is_string($value))
                $set_queries[] = "$key='$value'";
            else
                $set_queries[] = "$key=$value";
        }

        $q = "UPDATE Students SET ".implode(',',$set_queries)." WHERE id=".$updatedData['student_id'].";";

        if($conn->query($q))
        {
            return getStudentById($updatedData['student_id']);
        }
        else
        {
            echo $conn->error;
            return -1;
        }
    }

    function getStudentById($id)
    {
        global $conn;
        $q = "SELECT * FROM Students WHERE id=$id;";
        
        if(!$row = $conn->query($q))
        {
            return null;
        }

        $row = $row->fetch_assoc();

        $student = new Student(
            intval($row['id']),
            $row['first_name'],
            $row['last_name'],
            intval($row['group']),
            intval($row['gender']),
            intval($row['status']),
            $row['birthdate']
        );
        

        return $student;
    }

    function getAllStundents()
    {
        global $conn;
        $q = "SELECT * FROM Students;";
        
        if(!$result = $conn->query($q))
        {
            return null;
        }

        $students = array();
        foreach($result as $row)
        {
            $students[] = new Student(
                intval($row['id']),
                $row['first_name'],
                $row['last_name'],
                intval($row['group']),
                intval($row['gender']),
                intval($row['status']),
                $row['birthdate']
            );
        }

        return $students;
    }

    function escapedString($str)
    {
        global $conn;
        return $conn->real_escape_string($str);
    }

    function deleteStudent($id)
    {
        global $conn;
        $q = "DELETE FROM Students WHERE id=$id;";
        
        return boolval($conn->query($q));
    }
?>
