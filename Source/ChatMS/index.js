import { createServer } from 'http';

const httpServer = createServer();
import { randomUUID } from 'crypto';
import { chatModel, userModel, chatToUserRelationModel, messageModel, db } from "./models.js";

await db.connect('mongodb+srv://PviArtemMarushchak:jh8jzK8Sl8uVTyE7@pvilab.m8lpciv.mongodb.net/?retryWrites=true&w=majority');

import { Server } from "socket.io"; 

const io = new Server(httpServer, {
  cors: {
    origin: "http://localhost",
  },
});

io.use((socket, next) => {
  const username = socket.handshake.auth.username;
  if(!username)
    return next(new Error("Invalid username"));
  socket.username = username;
  socket.join(socket.username);
  next();
});

io.on("connection", async (socket) => {

  const user = await userModel.findOne({username:socket.username}).exec();
  console.log(user);
  if(user)
  {
    const chatRelations = await chatToUserRelationModel.find({username: socket.username}).exec();
    const chatIds = chatRelations.map(chatRel => chatRel.chatUUID);
    const chats = await chatModel.find().where('chatUUID').in(chatIds).exec();

    chats.forEach(chat => 
      {
        socket.emit("join request", {chatUUID: chat.chatUUID, chatname: chat.chatname});
      });

    const messages = await messageModel.find().where('to').in(chatIds).limit(20).exec();
    messages.forEach(message => 
      {
        socket.emit("incoming message", {from: message.from, to: message.to, content: message.content});
      });
  }
  else
  {
    const user = new userModel({username:socket.username});
    await user.save();
  }

  socket.emit("users", getAllUsers());
  socket.broadcast.emit("user connected", {id: socket.id, username: socket.username});
  socket.on("disconnect", () => socket.emit("user disconnected", {id: socket.id, username: socket.username}));

  socket.on("create chat", async ({participants, chatname}) =>
  {
    console.log("Creating chat!");
    const chatUUID = randomUUID();
    const relationSavingTasks = [];

    participants.forEach(participant => {
      socket.to(participant.username).emit("join request", {chatUUID, chatname});
      const relation = new chatToUserRelationModel({chatUUID, username: participant.username});
      relationSavingTasks.push(relation.save());
    });
    socket.emit("join request", {chatUUID, chatname});

    const chat = new chatModel({chatUUID, chatname});
    const chatSavePromise = chat.save();

    await Promise.all([...relationSavingTasks, chatSavePromise]);
  });

  socket.on("joined chat", ({chatUUID}) => {
    socket.join(chatUUID);
    socket.to(chatUUID).emit("joined chat", {id: socket.id, username: socket.username});
  });

  socket.on("send message", async ({from, to, content}) => {
    socket.to(to).emit("incoming message", {from, to, content});

    const messageObj = new messageModel({from, to, content});
    messageObj.save();
  });
});



httpServer.listen(4000);

function getAllUsers()
{
  const users = [];
  for (let [id, socket] of io.of("/").sockets) {
    users.push({
      id,
      username: socket.username,
    });
  }
  return users;
}