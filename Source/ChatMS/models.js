import mongoose, { Schema, model } from "mongoose";

const db = mongoose;

const chatSchema = new Schema({
    chatUUID: String,
    chatname: String,
});

const userSchema = new Schema({
    username: String,
})

const chatToUserRelationSchema = new Schema({
    username: String,
    chatUUID: String,
});

const messageSchema = new Schema({
    from: String,
    to: String,
    content: String,
});

const chatModel = model("Chat", chatSchema);
const userModel = model("User", userSchema);
const chatToUserRelationModel = model("ChatToUserRelation", chatToUserRelationSchema);
const messageModel = model("Message", messageSchema);

export { chatModel, userModel, chatToUserRelationModel, messageModel, db }