<?php
    $errors = [
        1 => "Invalid student's id",
        2 => "Invalid student's group",
        3 => "Invalid student's gender",
        4 => "Invalid student's first name",
        5 => "Invalid student's last name",
        6 => "Invalid student's status",
        7 => "Invalid student's birthdate",
        8 => "Connection error"
    ];

    function get_error_by_code($code)
    {
        global $errors;
        if(isset($errors[$code]))
            return $errors[$code];
        else
            return '';
    }
?>